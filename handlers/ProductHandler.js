'use strict';

const fs = require('fs');
const _ = require('lodash');
const watermark = require('purejswatermark');

const { Product, Store } = require('./../data/models');
const { ErrorMessages } = require('./../constants');
const { Thumbnail } = require('./../components');

const config = require('./../config');

class ProductHandler {
    static async actionIndex(ctx) {
        let {product_id} = ctx.params;

        const product = await Product.findByPk(product_id);

        if (_.isEmpty(product)) {
            return ctx.notFound(ErrorMessages.PRODUCT_NOT_FOUND)
        }

        return ctx.ok({image: product.image})
    }

    static async actionCreate(ctx) {
        const {product_id} = ctx.params;
        const {extensions, size, dimension} = config.get('params:validation:files:media:allowed');
        let media = _.get(ctx.request, 'files.media');

        if (_.isEmpty(media)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_.includes(extensions, _.get(media, 'ext'))) {
            return ctx.unsupportedMediaType(ErrorMessages.INVALID_FILE_TYPE);
        } else if (_.get(media, 'size') > size) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
        }

        const product = await Product.findByPk(product_id);

        if (_.isEmpty(product)) {
            return ctx.notFound(ErrorMessages.PRODUCT_NOT_FOUND)
        }

        const store = await Store.findByPk(product.store_id)

        const thumbnail = await Thumbnail.image(media, dimension / 2);

        const thumbnailKey = `media/thumbnails/${_.get(media, 'key')}`;

        const thumbnailBufferWithWatermark = await watermark.addWatermark(
            thumbnail.path,
            store.watermark_image,
            {
                dstPath: thumbnailKey
            }
        );
        fs.writeFileSync(thumbnailKey, thumbnailBufferWithWatermark);

        const mediaKey = `media/${_.get(media, 'key')}`;
        const mediaBuffer = fs.readFileSync(_.get(media, 'path'));

        fs.writeFileSync(mediaKey, mediaBuffer);

        product.image = mediaKey

        return ctx.ok({ product })
    }

    static async actionRemove(ctx) {
        const {product_id} = ctx.params;

        const product = await Product.findByPk(product_id);

        if (_.isEmpty(product)) {
            return ctx.notFound(ErrorMessages.PRODUCT_NOT_FOUND)
        }

        product.image = '';

        await product.save();

        return ctx.accepted();
    }

    static async actionThumbnail(ctx) {
        const {product_id} = ctx.params;

        const product = await Product.findByPk(product_id);

        if(_.isEmpty(product)) {
            return ctx.notFound(ErrorMessages.PRODUCT_NOT_FOUND)
        }

        const thumbnail = fs.readFileSync('media/thumbnails/' + product.image);

        return ctx.ok({ thumbnail })
    }
}

module.exports = ProductHandler;
