'use strict';

const fs = require('fs');
const _ = require('lodash');

const {Store, Product} = require('./../data/models');
const {ErrorMessages} = require('./../constants');
const {Security} = require('./../components');

const config = require('./../config');

class StoreHandler {
    static async actionIndex(ctx) {
        let {store_id} = ctx.params;

        const store = await Store.findByPk(store_id);

        if (_.isEmpty(store)) {
            return ctx.notFound(ErrorMessages.STORE_NOT_FOUND)
        }

        return ctx.ok({store})
    }

    static async actionCreate(ctx) {
        const {store_id} = ctx.params;
        const {extensions, size} = config.get('params:validation:files:watermark:allowed');
        let watermark = _.get(ctx.request, 'files.watermark');

        if (_.isEmpty(watermark)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_.includes(extensions, _.get(watermark, 'ext'))) {
            return ctx.unsupportedMediaType(ErrorMessages.INVALID_FILE_TYPE);
        } else if (_.get(watermark, 'size') > size) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
        }

        const store = await Store.findByPk(store_id);

        if (_.isEmpty(store)) {
            return ctx.notFound(ErrorMessages.STORE_NOT_FOUND)
        }

        const watermarkKey = `media/watermark/${_.get(watermark, 'key')}`;
        const watermarkBuffer = fs.readFileSync(_.get(watermark, 'path'));

        fs.writeFileSync(watermarkKey, watermarkBuffer);

        store.watermark_image = watermarkKey;

        await store.save();
        Security.queueGenerator(store)

        return ctx.ok({ store })
    }

    static async actionRemove(ctx) {
        const {store_id} = ctx.params;

        const store = await Store.findByPk(store_id);

        if (_.isEmpty(store)) {
            return ctx.notFound(ErrorMessages.STORE_NOT_FOUND)
        }

        store.watermark_image = '';

        await store.save();

        return ctx.accepted();
    }
}

module.exports = StoreHandler;
