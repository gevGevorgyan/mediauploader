'use strict';

const _get = require('lodash/get');
const _toNumber = require('lodash/toNumber');

function paginate({ limit: limitConfig, offset: offsetConfig }) {
    return async (ctx, next) => {
        let { limit, offset, page } = ctx.query;

        const pageValue = _toNumber(page >= 1 ? page : 1);
        const minLimitValue = _get(limitConfig, 'min', 2);
        const maxLimitValue = _get(limitConfig, 'max', 100);
        const minOffsetValue = _get(offsetConfig, 'min', 0);

        limit = _toNumber(limit >= minLimitValue && limit <= maxLimitValue ? limit : 10);
        offset = page ? (pageValue - 1) * limit : _toNumber(Math.max(minOffsetValue, offset)) || minOffsetValue;

        ctx.state.paginate = { limit, offset, page: pageValue };

        await next();
    };
}

module.exports = paginate;
