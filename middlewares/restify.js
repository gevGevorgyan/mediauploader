'use strict';

const respond = require('koa-respond');
const compose = require('koa-compose');

module.exports = params =>
    compose([
        respond({
            statusMethods: {
                ok: 200,
                accepted: 202,
                noContent: 204,
                movedPermanently: 301,
                movedTemporarily: 302,
                notModified: 304,
                unsupportedMediaType: 415,
                unprocessableEntity: 422
            }
        })
    ]);
