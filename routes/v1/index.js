'use strict';

const Router = require('koa-router');

const productHandlerRoute = require('./product');
const storeHandlerRoute = require('./store');

const router = new Router({
    prefix: '/v1'
});

router.use(storeHandlerRoute.routes());
router.use(productHandlerRoute.routes());

router.get('/ping', ctx => ctx.ok('pong'));

module.exports = router;
