'use strict';

const Router = require('koa-router');

const ProductHandler = require('../../handlers/ProductHandler');


const router = new Router({
    prefix: '/product'
});


router.get('/:product_id/image', ProductHandler.actionIndex);
router.get('/:product_id/thumbnail', ProductHandler.actionThumbnail);

router.post('/:product_id/image', ProductHandler.actionCreate);

router.delete('/:product_id/image', ProductHandler.actionRemove);

module.exports = router;
