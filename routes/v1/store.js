'use strict';

const Router = require('koa-router');

const StoreHandler = require('../../handlers/StoreHandler');


const router = new Router({
    prefix: '/store'
});


router.get('/:store_id/watermark', StoreHandler.actionIndex);

router.post('/:store_id/watermark', StoreHandler.actionCreate);

router.delete('/:store_id/watermark', StoreHandler.actionRemove);

module.exports = router;
