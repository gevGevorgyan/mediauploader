'use strict';

const os = require('os');
const fs = require('fs');
const sharp = require('sharp');
const _get = require('lodash/get');
const ffmpeg = require('fluent-ffmpeg');
const _toString = require('lodash/toString');
const ffmpegInstaller = require('@ffmpeg-installer/ffmpeg');

const Security = require('./Security');
const config = require('../config');

ffmpeg.setFfmpegPath(ffmpegInstaller.path);

class Thumbnail {
    static async image(file, dimension) {
        try {
            const path = `${os.tmpdir()}/${Security.generateRandomString()}.${_get(file, 'ext')}`;

            await sharp(_get(file, 'path'))
                .jpeg({ progressive: true, force: false })
                .png({ progressive: true, force: false })
                .rotate()
                .resize(dimension || config.get('params:validation:files:dimension'))
                .toFile(path);

            const size = _toString(fs.statSync(path).size);

            return { ...file, size, path };
        } catch (e) {
            console.log(e);
        }

        return file;
    }

    static async video(file) {
        try {
            ffmpeg(file.path)
                .takeScreenshots({
                    timemarks: [ '0', '5', '7' ],
                    filename: `${_get(file.key.split('.'), 0)}-%s-seconds.png`,
                    folder: 'media/thumbnails'
                });
        } catch (e) {
            console.log(e);
        }

        return file;
    }
}

module.exports = Thumbnail;
