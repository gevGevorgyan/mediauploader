'use strict';

const fs = require('fs');
const kue = require('kue');
const randomString = require('randomstring');
const watermark = require('purejswatermark');

const queue = kue.createQueue({redis: process.env.REDIS_URL});
const { Product } = require('./../data/models');


class Security {
    /**
     * @param options
     */
    static generateRandomString(options = 16) {
        return randomString.generate(options);
    }


    /**
     * @param id
     * @param image
     */
    static queueGenerator(id, image) {
        queue.process('watermark', async () => {
            const products = Product.findAll({
                where: {
                    store_id: id
                }
            })

            if(!_.isEmpty(products)) {
                products.map(async product => {
                    const thumbnailKey = `media/thumbnails/${product.image}`;
                    const thumbnailBufferWithWatermark = await watermark.addWatermark(
                        thumbnailKey,
                        image,
                        {
                            dstPath: thumbnailKey
                        }
                    );

                    fs.writeFileSync(thumbnailKey, thumbnailBufferWithWatermark);
                })

                return products
            }
        })
    }
}

module.exports = Security;
