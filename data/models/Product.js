'use strict';

module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define(
        'Product',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            store_id: {
                type: DataTypes.UUID
            },
            title: {
                type: DataTypes.STRING
            },
            image: {
                type: DataTypes.STRING
            }
        },
        {
            tableName: 'product',
            timestamps: true
        }
    );

    return Product;
};