'use strict';

module.exports = (sequelize, DataTypes) => {
    const Store = sequelize.define(
        'Store',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            title: {
                type: DataTypes.STRING
            },
            watermark_image: {
                type: DataTypes.STRING
            }
        },
        {
            tableName: 'store',
            timestamps: true
        }
    );

    return Store;
};