'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('store', {
            id: {
                primaryKey: true,
                type: Sequelize.UUID
            },
            store_id: {
                type: Sequelize.UUID,
                onDelete: 'CASCADE',
                references: {
                    model: 'store',
                    key: 'id'
                }
            },
            title: {
                type: Sequelize.STRING
            },
            image: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },

    async down(queryInterface) {
        await queryInterface.dropTable('store')
    }
};
