'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('store', {
            id: {
                primaryKey: true,
                type: Sequelize.UUID
            },
            title: {
                type: Sequelize.STRING
            },
            watermark_image: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },

    async down(queryInterface) {
        await queryInterface.dropTable('store')
    }
};
