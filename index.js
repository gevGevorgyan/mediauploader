'use strict';

const Koa = require('koa');
const app = new Koa();

const config = require('./config/index');

/**
 * ############## MIDDLEWARES ##############
 */
app.use(require('@koa/cors')());
app.use(require('koa-static')('public'));
app.use(
    require('./middlewares/requestNormalizer')({
        bodyParser: {
            multipart: true,
            formidable: {
                maxFileSize: config.get('params:validation:maxFileSize')
            }
        },
        paginate: {
            limit: config.get('request:limit') || 5
        }
    })
);
app.use(
    require('./middlewares/restify')({
        serializer: {
        }
    })
);

/**
 * ############## ROUTES ##############
 */
const v1Routes = require('./routes/v1');

app.use(v1Routes.routes());
app.use(v1Routes.allowedMethods());

/**
 * ############## SERVER CONFIGURATION ##############
 */
const port = config.get('server:port');
const server = require('http').createServer(app.callback());

server.listen(port, () => {
    console.info(`Server is running on port : ${port}`);
});
