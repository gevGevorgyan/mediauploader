'use strict';

module.exports = {
    FILE_REQUIRED: 'err_file_required',
    INVALID_FILE_TYPE: 'err_invalid_file',
    INVALID_FILE_SIZE: 'err_invalid_size',
    STORE_NOT_FOUND: 'err_store_not_found',
    PRODUCT_NOT_FOUND: 'err_product_not_found'
};
